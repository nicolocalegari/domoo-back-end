var macPort = "/dev/tty.usbmodem1411";
var iMacPort = "/dev/tty.usbmodemfd141";
var raspiPort = "/dev/ttyACM0";
var Firebase = require("firebase");
var _ = require('lodash');
var CronJob = require('cron').CronJob;


//SETTING UP SERIAL PORTS

var serialport = require("serialport");
var SerialPort = serialport.SerialPort;
var myFirebaseRef = new Firebase("https://burning-fire-8878.firebaseio.com/");
var dataRef = myFirebaseRef.child('data');
var hourRef = myFirebaseRef.child('hour');
var dayAverageRef = myFirebaseRef.child('day');
var daysOfWeek = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

var sp = new SerialPort(iMacPort, {
  baudrate: 115200,
  databits: 8,
  parser: serialport.parsers.readline('\n')
});

var sensorsData = {};

sp.on("open", function(){
  console.log('open');
  sp.on('data', function(data) {
    
    arrayData = splitData(data);

    // split single array element in single pure data
    arrayData.forEach(function(elem){
      var singleData = elem.split(':');

      if(singleData[0] === 'airHumidity'){
        sensorsData.airHumidity = singleData[1];
      }
      if(singleData[0] === 'temp'){
        sensorsData.temperature = singleData[1];
      }
      if(singleData[0] === 'ill'){
        sensorsData.illumination = singleData[1];
      }
      if(singleData[0] === 'hum'){
        sensorsData.soilHumidity = singleData[1];
      }
      if(singleData[0] === 'touch'){
        sensorsData.touch = singleData[1];
      }
    });

    // write data to Firebase realtime (with .set() method)
    dataRef.set(sensorsData);

  });

  var hourValues = new CronJob('00 30 * * * *', function(){
    setTimeInfo();
    pushData();
  }, null, true);

  hourRef.on('value', function(snapshot){
      var elements = snapshot.numChildren();
      if(elements > 24){
        hourRef.limitToFirst(1).once('child_added', function(firstElem){
          hourRef.child(firstElem.key()).remove();
          console.log('Removed 24 hours ago value');
        });
      }else{ 
        console.log('Still under 24h data');
      }
    });

  var dayValues = new CronJob('00 55 23 * * *', function(){
    hourRef.once('value', function(snapshot){
      var yesterdayValuesArray = [];
      var plainValues = (snapshot.val());
        for (var key in plainValues) {
          if (plainValues.hasOwnProperty(key)){
            var singleObj = plainValues[key];
            yesterdayValuesArray.push(singleObj);
          }
        }
      calculateAverageValue(yesterdayValuesArray);
      console.log('I ve been called');
      yesterdayValuesArray.length = 0;
    })
  });

  dayValues.start();

  // every hour I push data and set a new global date to make comparison: if sensorsData.dayOfMonth is minor than today,
  // I query the db asking for all yesterday values, I push em into an empty array and pass it to calculateAverage function
  /*setInterval(function(){
    var time = new Date();
    var today = time.getDate();
    var yesterdayValuesArray = [];

    pushData();

    hourRef.once('value', function(snapshot){
      var elements = snapshot.numChildren();
      if(elements > 24){
        hourRef.limitToFirst(1).once('child_added', function(firstElem){
          hourRef.child(firstElem.key()).remove();
        });
      }else{ 
        console.log('Still under 24h data');
      }
    });

    hourRef.orderByChild('dayOfMonth').equalTo(today - 1).on('value', function(snapshot){
      if(today != sensorsData.dayOfMonth && sensorsData.dayOfMonth != null){

        var plainValues = (snapshot.val());
        for (var key in plainValues) {
          if (plainValues.hasOwnProperty(key)){
            var singleObj = plainValues[key];
            yesterdayValuesArray.push(singleObj);
          }
        }
        calculateAverageValue(yesterdayValuesArray);
      } else {
        console.log('today is still today...');
      }
    });

    console.log('data pushed correctly');

    }, 3600000);
*/
  // set time attributes to sensorsData object every hour, with an hour and one second of delay to avoid mismatch in calculation

  console.log('Swag happens');

});

//HOISTED FUNCTIONS

// split Arduino data in array elements
function splitData(string){
  res = string.split(',');
  return res;
}

// sets date info to sensorsData object
function setTimeInfo(){
  var time = new Date();
  var hour = time.getHours();
  var day
  sensorsData.hour = hour;
}

// v2: if day is over, log the average values of sensor for this day, and reset the array for... a new day!
function pushData(){
  hourRef.push(sensorsData);
  console.log('Pushed hourly value');
}

// calculate average value for each sensor
function calculateAverageValue(dailyValuesArray){
  var dailyTemp = [];
  var dailyAirHumidity = [];
  var dailyIllumination = [];
  var dailySoilHumidity = [];
  var dailyPot = [];
  averageSensorsData = {};

  var time = new Date();
  var day = daysOfWeek[time.getDay()];
  averageSensorsData.day = day;

  dailyValuesArray.forEach(function(sensor){
    dailyTemp.push(parseFloat(sensor.temperature));
    dailyAirHumidity.push(parseInt(sensor.airHumidity));
    dailyIllumination.push(parseInt(sensor.illumination));
    dailySoilHumidity.push(parseInt(sensor.soilHumidity));
    dailyPot.push(parseInt(sensor.potentiometer));
  });

  var sumTemp = _.sum(dailyTemp);
  averageSensorsData.temperature = parseFloat(sumTemp / dailyTemp.length).toFixed(1).toString();
  var sumAirHum = _.sum(dailyAirHumidity);
  averageSensorsData.airHumidity = parseInt(sumAirHum / dailyAirHumidity.length).toString();
  var sumIll = _.sum(dailyIllumination);
  averageSensorsData.illumination = parseInt(sumIll / dailyIllumination.length).toString();
  var sumSoilHum = _.sum(dailySoilHumidity);
  averageSensorsData.soilHumidity = parseInt(sumSoilHum / dailySoilHumidity.length).toString();
  var sumPot = _.sum(dailyPot);
  averageSensorsData.potentiometer = parseInt(sumPot / dailyPot.length).toString();
  dayAverageRef.push().set(averageSensorsData);
  console.log('average data calculated and pushed');
}